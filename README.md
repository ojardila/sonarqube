Instalación de SonarQube
==============


SonarQube (conocido anteriormente como Sonar1​) es una plataforma para evaluar código fuente. Es software libre y usa diversas herramientas de análisis estático de código fuente como Checkstyle, PMD o FindBugs para obtener métricas que pueden ayudar a mejorar la calidad del código de un programa.2


|    Version      | Tag     | Puerto Web  |
|-----------------|---------|-------------|
|   7.9.2.30863   |    -    | 9000        |


Uso
-----
1. Edite el archivo docker-compose.yml cambiando las variables necesarias.l
Simplemente ejecute:

```
docker-compose up -d

```

#### Soporte de volúmenes
Dentro de su carpeta de sonarqube existen carpetas  la cuáles serán montada dentro del contenedor en los directorios

```
  sonarqube_conf:
  sonarqube_data:
  sonarqube_extensions:
  postgresql:
  postgresql_data:

```

Los directorios deben existir antes de correr el comando de instalación.
